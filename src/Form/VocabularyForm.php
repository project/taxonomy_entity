<?php

namespace Drupal\taxonomy_entity\Form;

use Drupal\taxonomy\VocabularyForm as CoreVocabularyForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\VocabularyInterface;

/**
 * Base form for vocabulary edit forms.
 */
class VocabularyForm extends CoreVocabularyForm {

  /**
   * The vocabulary storage.
   *
   * @var \Drupal\taxonomy\VocabularyStorageInterface
   */
  protected $vocabularyStorage;

  /**
   * Form element validation handler for hierarchy vocabulary element.
   */
  public static function validateHierarchy(&$element, FormStateInterface $form_state, &$complete_form): void {
    /** @var VocabularyForm $form */
    $form = $form_state->getFormObject();
    $vocabulary = $form->getEntity();

    /** @var \Drupal\taxonomy\Entity\Term[] $tree */
    $tree = \Drupal::service('entity_type.manager')
      ->getStorage('taxonomy_term')
      ->loadTree($vocabulary->id());
    $vocabularyHierarchy = VocabularyInterface::HIERARCHY_DISABLED;
    foreach ($tree as $term) {
      // Check term depth to check vocabulary hierarchy.
      if ($term->depth >= VocabularyInterface::HIERARCHY_MULTIPLE) {
        $vocabularyHierarchy = VocabularyInterface::HIERARCHY_MULTIPLE;
        break;
      }
      elseif ($term->depth == VocabularyInterface::HIERARCHY_SINGLE) {
        $vocabularyHierarchy = VocabularyInterface::HIERARCHY_SINGLE;
      }
    }

    if ($element['#value'] > $vocabularyHierarchy) {
      return;
    }

    // If you want to move from a multiple vocabulary to single.
    if (
      $vocabularyHierarchy === VocabularyInterface::HIERARCHY_MULTIPLE &&
      $element['#value'] == VocabularyInterface::HIERARCHY_SINGLE
    ) {
      $form_state->setError($element, t('This cannot be changed to the chosen setting because existing terms already have a relational pattern of children with a multiple parent.'));
      return;
    }

    // If you want to move from a single/multiple vocabulary to hierarchy disabled.
    if (
      ($vocabularyHierarchy === VocabularyInterface::HIERARCHY_SINGLE
        || $vocabularyHierarchy === VocabularyInterface::HIERARCHY_MULTIPLE)
      && $element['#value'] == VocabularyInterface::HIERARCHY_DISABLED
    ) {
      $form_state->setError($element, t('This cannot be changed to the chosen setting because existing terms already have a relational pattern of children with a multiple or single parent.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\taxonomy\Entity\Vocabulary $vocabulary */
    $vocabulary = $this->entity;
    // Update the current hierarchy type as we go.
    $termStorage = $this->entityTypeManager->getStorage('taxonomy_term');

    $form['hierarchy'] = [
      '#type' => 'select',
      '#title' => $this->t('Hierarchy'),
      '#default_value' => $vocabulary->id() !== null ? $termStorage->getVocabularyHierarchyType($vocabulary->id()) : NULL,
      '#options' => [
        VocabularyInterface::HIERARCHY_DISABLED => $this->t('No hierarchy'),
        VocabularyInterface::HIERARCHY_SINGLE => $this->t('Single parent hierarchy'),
        VocabularyInterface::HIERARCHY_MULTIPLE => $this->t('Multiple parent hierarchy'),
      ],
      '#element_validate' => [
        [$this, 'validateHierarchy'],
      ],
    ];

    return $this->protectBundleIdElement($form);
  }

  /**
   * {@inheritDoc}
   */
  public function save(array $form, FormStateInterface $form_state): void {
    $termStorage = $this->entityTypeManager->getStorage('taxonomy_term');

    /** @var \Drupal\taxonomy\Entity\Vocabulary $vocabulary */
    $vocabulary = $this->entity;
    parent::save($form, $form_state);
    $termStorage->setVocabularyHierarchyType($vocabulary->id(), $form_state->getValue('hierarchy'));
  }

}
