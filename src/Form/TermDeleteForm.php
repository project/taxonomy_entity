<?php

namespace Drupal\taxonomy_entity\Form;

use Drupal\taxonomy\Form\TermDeleteForm as CoreTermDeleteForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a deletion confirmation form for taxonomy term.
 */
class TermDeleteForm extends CoreTermDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->entityTypeManager->getStorage('taxonomy_term')->resetCache();
  }

}
