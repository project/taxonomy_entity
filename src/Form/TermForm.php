<?php

namespace Drupal\taxonomy_entity\Form;

use Drupal\taxonomy\TermForm as CoreTermForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\VocabularyInterface;

/**
 * Base for handler for taxonomy term edit forms.
 */
class TermForm extends CoreTermForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $term = $this->entity;
    $vocabularyStorage = $this->entityTypeManager->getStorage('taxonomy_vocabulary');
    /** @var \Drupal\taxonomy\Entity\Vocabulary $vocabulary */
    $vocabulary = $vocabularyStorage->load($term->bundle());

    // Update the current hierarchy type as we go.
    $termStorage = $this->entityTypeManager->getStorage('taxonomy_term');
    $hierarchy = (int) $termStorage->getVocabularyHierarchyType($vocabulary->id());

    if ($hierarchy === VocabularyInterface::HIERARCHY_DISABLED) {
      $form['relations']['#access'] = FALSE;
    }

    // Loads options tree for single hierarchy.
    if ($hierarchy === VocabularyInterface::HIERARCHY_SINGLE) {
      /** @var \Drupal\taxonomy\Entity\Term[] $tree */
      $tree = $this->entityTypeManager->getStorage('taxonomy_term')->loadTree($vocabulary->id());
      $options = [
        0 => "<root>",
      ];
      foreach ($tree as $term) {
        if ($term->depth >= 1) {
          continue;
        }
        $options[$term->revision_id] = $term->name;
      }

      $form['relations']['parent']['#options'] = $options;
      $form['relations']['parent']['#multiple'] = FALSE;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildEntity(array $form, FormStateInterface $form_state) {
    $tempParentValue = $form_state->getValue('parent');

    if (!is_array($tempParentValue)) {
      $form_state->setValue('parent', [$tempParentValue]);
    }

    $term = parent::buildEntity($form, $form_state);

    $vocabularyStorage = $this->entityTypeManager->getStorage('taxonomy_vocabulary');
    /** @var \Drupal\taxonomy\Entity\Vocabulary $vocabulary */
    $vocabulary = $vocabularyStorage->load($term->bundle());

    // Update the current hierarchy type as we go.
    $termStorage = $this->entityTypeManager->getStorage('taxonomy_term');
    $hierarchy = $termStorage->getVocabularyHierarchyType($vocabulary->id());
    if ($hierarchy === VocabularyInterface::HIERARCHY_SINGLE) {
      // Assign parents with proper delta values starting from 0.
      $term->parent = $form_state->getValue('parent');
    }

    return $term;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    parent::save($form, $form_state);
    $this->entityTypeManager->getStorage('taxonomy_term')->resetCache();
  }

}
