<?php

namespace Drupal\taxonomy_entity;

use Drupal\taxonomy\TermStorage as CoreTermStorage;
/**
 * Defines a Controller class for taxonomy terms.
 */
class TermStorage extends CoreTermStorage {

  /**
   * {@inheritdoc}
   */
  public function getVocabularyHierarchyType($vid) {
    if (!isset($this->vocabularyHierarchyType[$vid])) {
      /** @var \Drupal\taxonomy\Entity\Vocabulary $vocabulary */
      $vocabulary = $this->entityTypeManager->getStorage('taxonomy_vocabulary')->load($vid);
      $hierarchyType = $vocabulary->getThirdPartySetting('taxonomy_entity', 'hierarchy');

      if (!empty($hierarchyType)) {
        $this->vocabularyHierarchyType[$vid] = $hierarchyType;
      }
    }

    return parent::getVocabularyHierarchyType($vid);
  }

  /**
   * Set the vocabulary hierarchy type.
   *
   * @param int $vid
   * @param int $hierarchy
   *
   * @return void
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function setVocabularyHierarchyType($vid, $hierarchy) {
    /** @var \Drupal\taxonomy\VocabularyInterface $vocabulary */
    $vocabulary = $this->entityTypeManager->getStorage('taxonomy_vocabulary')->load($vid);
    $vocabulary->setThirdPartySetting('taxonomy_entity', 'hierarchy', $hierarchy);
    $vocabulary->save();
    unset($this->vocabularyHierarchyType[$vid]);
  }
}
